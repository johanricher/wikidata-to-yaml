'use strict';

const co = require('co');
const fs = require('mz/fs');
const https = require('https');
const path = require('path');
const program = require('commander');
const yaml = require('js-yaml');
const _ = require('lodash');

co(function * () {
  var dataDir;

  program
    .version(require('./package.json').version)
    .usage('<outputDir>')
    .arguments('<outputDir>')
    .action(outputDir => {
      dataDir = outputDir;
    })
    .parse(process.argv);

  if (typeof dataDir === 'undefined') {
    console.error('No output directory given.');
    process.exit(1);
  }

  var stats;
  try {
    stats = yield fs.stat(dataDir);
  } catch (e) {
    if (e.code === 'ENOENT') {
      console.error('Output directory does not exists.');
      process.exit(1);
    }
  }

  if (!stats.isDirectory()) {
    console.error(dataDir + ' is not a directory.');
    process.exit(1);
  }

  var postData = (yield fs.readFile('./request.sparql')).toString();

  var querystring = require('querystring').stringify({query: postData});
  var options = {
    hostname: 'query.wikidata.org',
    port: 443,
    path: '/bigdata/namespace/wdq/sparql?' + querystring,
    method: 'GET',
    headers: {
      Accept: 'application/json'
    }
  };

  var req = https.request(options, res => {
    console.log('Receiving data...');
    var results = '';
    res.setEncoding('utf8');
    res.on('data', chunk => {
      results += chunk;
    });

    res.on('end', () => {
      console.log('Writing results to ' + dataDir + ' ...');
      var resultsObject;
      try {
        resultsObject = JSON.parse(results);
      } catch (e) {
        console.log(e);
        throw e;
      }

      results = resultsObject.results.bindings;

      results = results.reduce(function(mergedResults, toMerge) {
        var label = (toMerge.program_enlabel || toMerge.program_frlabel);
        if (!label) {
          return mergedResults;
        }
        var softId = label.value.toLowerCase().replace(/[^a-z0-9]/g, '_');
        mergedResults[softId] = mergedResults[softId] || {};
        Object.keys(toMerge).forEach(function(property) {
          mergedResults[softId][property] = mergedResults[softId][property] || [];
          if (_.findIndex(mergedResults[softId][property], toMerge[property]) === -1) {
            mergedResults[softId][property].push(toMerge[property]);
          }
        });

        return mergedResults;
      }, {});

      var directories = {};
      Object.keys(results).map(co.wrap(function * (softId) {
        var fileName = softId + '.yaml';

        if (!directories[fileName.slice(0, 1)]) {
          try {
            fs.statSync(path.join(dataDir, fileName.slice(0, 1)));
          } catch (e) {
            if (e.code === 'ENOENT') {
              fs.mkdirSync(path.join(dataDir, fileName.slice(0, 1)));
              console.log('Creating folder for letter ' + fileName.slice(0, 1));
            }
          }
        }
        directories[fileName.slice(0, 1)] = true;

        fs.writeFileSync(
          path.join(dataDir, fileName.slice(0, 1), fileName),
          yaml.dump(results[softId], {sortKeys: true})
        );
      }));
    });
  });

  req.on('error', err => {
    console.log(err);
  });

  req.end();
  console.log('Waiting for response...');
});
